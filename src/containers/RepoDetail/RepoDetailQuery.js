import gql from 'graphql-tag';

const RepoDetailQuery = gql`
  query GetRepoDetil($id: ID!) {
    node(id: $id) {
      ... on Repository {
        id
        name
        owner {
          id
          avatarUrl(size: 48)
          login
        }
        forkCount
        stargazers {
          totalCount
        }
        watchers {
          totalCount
        }
      }
    }
  }
`;

export default RepoDetailQuery;
