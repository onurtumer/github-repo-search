import React from 'react';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from 'react-apollo';

import Search from './containers/Search';

import logo from './logo.svg';
import './App.css';

// Yes, this is an unsafe way ;)
const TOKEN = '458abb476b15af0df5841d07380be1109f485f02'; // <-- TODO: place your token here

const client = new ApolloClient({
  link: new HttpLink({
    uri: 'https://api.github.com/graphql',
    headers: {
      Authorization: `token ${TOKEN}`,
    },
  }),
  cache: new InMemoryCache(),
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to github repository finder</h1>
        </header>
        <Search />
      </div>
    </ApolloProvider>
  );
};

export default App;
