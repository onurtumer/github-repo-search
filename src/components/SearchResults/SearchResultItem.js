/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events  */
import React from 'react';

// TODO: Make keyboard accessible

const SearchResultItem = ({ repo, onClick, onSave, onDelete }) => {
  const handleSave = e => {
    e.stopPropagation();
    onSave(repo);
  };

  const handleDelete = e => {
    e.stopPropagation();
    onDelete(repo.id);
  };

  const handleItemClick = () => {
    if (!onClick) {
      return;
    }

    onClick(repo.id);
  };

  return (
    <div className="SearchResults__item" onClick={handleItemClick}>
      {repo.name}
      {onSave && (
        <button type="button" onClick={handleSave}>
          Save
        </button>
      )}
      {onDelete && (
        <button type="button" onClick={handleDelete}>
          Delete
        </button>
      )}
    </div>
  );
};

export default SearchResultItem;
/* eslint-enable */
