import React from 'react';
import cx from 'classnames';

import './RepoDetailItem.css';

const RepoDetailItem = ({ className, title, value }) => (
  <div className={cx('RepoDetailItem', className)}>
    <div className="RepoDetailItem__title">{title}:</div>
    <div className="RepoDetailItem__value">{value.toLocaleString()}</div>
  </div>
);

export default RepoDetailItem;
