import React from 'react';
import cx from 'classnames';

import './LoadingIndicator.css';

const LoadingIndicator = ({ className }) => (
  <div className={cx('LoadingIndicator', className)}>
    <div className="LoadingIndicator__spinner" />
  </div>
);

export default LoadingIndicator;
