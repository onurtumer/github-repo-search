import React from 'react';

import SearchResultItem from './SearchResultItem';
import './SearchResults.css';

const SearchResults = ({ repos, onItemClick, onSave, onDelete }) => {
  return (
    <div className="SearchResults">
      {repos.map(repo => (
        <SearchResultItem
          key={repo.id}
          repo={repo}
          onClick={onItemClick}
          onSave={onSave}
          onDelete={onDelete}
        />
      ))}
    </div>
  );
};

export default SearchResults;
