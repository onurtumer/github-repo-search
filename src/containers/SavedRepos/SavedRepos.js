/* eslint-disable react/jsx-fragments */
import React, { Component, Fragment } from 'react';

import * as RepoStorage from '../../services/RepoStorage';

import SearchResults from '../../components/SearchResults';

import './SavedRepos.css';

class SavedRepos extends Component {
  state = {
    showRepos: false,
    repos: [],
  };

  componentDidMount() {
    const repos = RepoStorage.getRepos();
    this.setState({ repos });
    window.addEventListener(RepoStorage.updateEventName, this.onLocalStorageUpdate);
  }

  componentWillUnmount() {
    window.removeEventListener(RepoStorage.updateEventName, this.onLocalStorageUpdate);
  }

  onLocalStorageUpdate = () => {
    const repos = RepoStorage.getRepos();
    this.setState({ repos });
  };

  toggleShowRepos = () => {
    this.setState(prevState => ({ showRepos: !prevState.showRepos }));
  };

  handleDelete = repoId => {
    RepoStorage.removeRepo(repoId);
  };

  render() {
    const { repos, showRepos } = this.state;
    const { onItemClick } = this.props;

    return (
      <Fragment>
        <div className="SavedRepos">
          You have {repos.length} saved repositories.
          <button className="SavedRepos__button" type="button" onClick={this.toggleShowRepos}>
            {showRepos ? 'Hide' : 'Show'} Repos
          </button>
        </div>
        {showRepos && (
          <div className="SavedRepos__container">
            <SearchResults repos={repos} onItemClick={onItemClick} onDelete={this.handleDelete} />
          </div>
        )}
      </Fragment>
    );
  }
}

export default SavedRepos;
