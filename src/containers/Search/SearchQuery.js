import gql from 'graphql-tag';

const SearchQuery = gql`
  query SearchRepository($query: String!) {
    search(query: $query, type: REPOSITORY, first: 20) {
      nodes {
        ... on Repository {
          id
          name
        }
      }
    }
  }
`;

export default SearchQuery;
