const KEY = 'savedRepos';
const updateEventName = 'reposUpdated';

const dispatchUpdateEvent = () => {
  // eslint-disable-next-line no-undef
  const event = new Event(updateEventName);

  window.dispatchEvent(event);
};

const getRepos = () => {
  return JSON.parse(localStorage.getItem(KEY)) || [];
};

const updateRepos = updatedRepos => {
  localStorage.setItem(KEY, JSON.stringify(updatedRepos));
  dispatchUpdateEvent();
};

const addRepo = repo => {
  if (!repo || !repo.id || !repo.name) {
    return;
  }

  const savedRepos = getRepos();

  if (savedRepos.find(savedRepo => savedRepo.id === repo.id)) {
    return;
  }

  const updatedRepos = [...savedRepos, repo];
  updateRepos(updatedRepos);
};

const removeRepo = repoId => {
  if (!repoId || typeof repoId !== 'string') {
    return;
  }

  const savedRepos = getRepos();
  const updatedRepos = savedRepos.filter(repo => repoId !== repo.id);
  updateRepos(updatedRepos);
};

export { addRepo, removeRepo, getRepos, updateEventName };
