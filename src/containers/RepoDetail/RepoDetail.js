/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
import React, { PureComponent } from 'react';
import { graphql } from 'react-apollo';

import RepoDetailItem from '../../components/RepoDetailItem';
import LoadingIndicator from '../../components/LoadingIndicator';

import RepoDetailQuery from './RepoDetailQuery';
import './RepoDetail.css';

class RepoDetail extends PureComponent {
  render() {
    const { repoId, data, onOverlayClick } = this.props;

    if (!repoId) {
      return null;
    }

    if (data.loading) {
      return (
        <div className="RepoDetail">
          <LoadingIndicator />
        </div>
      );
    }

    if (!data || !data.node) {
      return <div>Ooops...</div>;
    }

    const { node } = data;
    const { owner } = node;

    return (
      <div className="RepoDetail" onClick={onOverlayClick}>
        <div className="RepoDetail__container" onClick={e => e.stopPropagation()}>
          <header className="RepoDetail__header">
            <div className="RepoDetail__imageContainer">
              <img
                className="RepoDetail__image"
                src={owner.avatarUrl}
                height="48"
                width="48"
                alt={owner.login}
              />
            </div>
            <div className="RepoDetail__info">
              <div className="RepoDetail__login">{owner.login}</div>
              <div className="RepoDetail__name">{node.name}</div>
            </div>
          </header>
          <div className="RepoDetail__content">
            <RepoDetailItem
              className="RepoDetail__item"
              title="Stars"
              value={node.stargazers.totalCount}
            />
            <RepoDetailItem className="RepoDetail__item" title="Forks" value={node.forkCount} />
            <RepoDetailItem
              className="RepoDetail__item"
              title="Watchers"
              value={node.watchers.totalCount}
            />
          </div>
        </div>
      </div>
    );
  }
}

const withRepoDetail = graphql(RepoDetailQuery, {
  options: props => ({
    variables: { id: props.repoId },
  }),
});

export default withRepoDetail(RepoDetail);
/* eslint-enable */
