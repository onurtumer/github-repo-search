/* eslint-disable react/jsx-fragments */
import React, { Component } from 'react';
import { ApolloConsumer } from 'react-apollo';

import LoadingIndicator from '../../components/LoadingIndicator';
import RepoDetail from '../RepoDetail';
import SearchResults from '../../components/SearchResults';
import SavedRepos from '../SavedRepos/SavedRepos';

import * as RepoStorage from '../../services/RepoStorage';

import SearchQuery from './SearchQuery';
import './Search.css';

class Search extends Component {
  state = {
    loading: false,
    searchTerm: '',
    repos: [],
    selectedRepo: null,
  };

  handleTermChange = e => {
    this.setState({ searchTerm: e.target.value });
  };

  handleSubmit = async (e, client) => {
    e.preventDefault();
    const { searchTerm } = this.state;

    this.setState({ loading: true, selectedRepo: null, repos: [] });
    try {
      const { data } = await client.query({
        query: SearchQuery,
        variables: { query: searchTerm },
      });
      this.setState({ repos: data.search.nodes });
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error('Error', error);
    } finally {
      this.setState({ loading: false });
    }
  };

  handleRepoClick = repoId => {
    this.setState({ selectedRepo: repoId });
  };

  handleOverlayClick = () => {
    this.setState({ selectedRepo: null });
  };

  handleSaveClick = repo => {
    RepoStorage.addRepo(repo);
  };

  render() {
    const { repos, loading, searchTerm, selectedRepo } = this.state;
    return (
      <div className="Search">
        <ApolloConsumer>
          {client => (
            <form className="Search__form" onSubmit={e => this.handleSubmit(e, client)}>
              <input
                className="Search__input"
                type="text"
                onChange={this.handleTermChange}
                value={searchTerm}
              />
              <button className="Search__button" type="submit">
                Search
              </button>
            </form>
          )}
        </ApolloConsumer>
        <SavedRepos onItemClick={this.handleRepoClick} />
        {loading && <LoadingIndicator className="Search__loading" />}
        {repos.length > 0 && (
          <SearchResults
            repos={repos}
            onItemClick={this.handleRepoClick}
            onSave={this.handleSaveClick}
          />
        )}
        {selectedRepo && (
          <RepoDetail repoId={selectedRepo} onOverlayClick={this.handleOverlayClick} />
        )}
      </div>
    );
  }
}

export default Search;
